//alert("Hi!");

// Addition
function addition(sum1, sum2) {
	let res1 = sum1 + sum2;
	console.log("Display sum of " + sum1 + " and " + sum2);
	console.log(res1);
}

addition(5, 15);


// Subtraction
function subtraction(sub1, sub2) {
	let res2 = sub1 - sub2;
	console.log("Displayed difference of " + sub1 + " and " + sub2);
	console.log(res2);
}

subtraction(20, 5);

// Multiplication
function multiplication(num5, num6) {
	let res3 = num5 * num6;
	console.log("The product of " + num5 + " and " + num6);
	return res3
}

let result3 = multiplication(50, 10);
console.log(result3);


// Division
function division(num7, num8) {
	let res4 = num7 / num8;
	console.log("The quotient of " + num7 + " and " + num8);
	return res4
}

let result4 = division(50, 10);
console.log(result4);


// Circle Radius
function circleRadius(num9) {
	let pie_value = (Math.PI * num9 **2);
	console.log("The result of getting the area of circle with " + num9 + " radius: ");
	return pie_value

}
let result5 = circleRadius(15)
console.log(result5)

// Average
function average(average1, average2, average3, average4) {
	let averageComputation = (average1 + average2 + average3 + average4) / 4;
	console.log("The total average of four numbers: " + average1 + "," + average2 + "," + average3 + "," + average4 + ":");
	return averageComputation
}
let newAverage = average(20, 40, 60, 80);
console.log(newAverage);

//Passing Score
function checkifPassed(score, total) {
	return (score/total)*100 > 75;
}
let isPassingScore =checkifPassed(38, 40);
console.log("Is 38/50 a passing score?")
console.log(isPassingScore);

